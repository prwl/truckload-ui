/**
 * Root Reducer
 */
import { combineReducers } from 'redux';

// Import Reducers
import signin from '../modules/public-modules/SignInForm/SignInFormReducer';

// Combine all reducers into one root reducer

export default combineReducers({
  signin,
});
