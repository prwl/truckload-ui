import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import SignInForm from './components/SignInForm';

import { setLoginFormDetails } from './SignInFormAction';

class SignInFormContainer extends Component {

  handleOnChange = (event) => {
    const { name, value, checked } = event.target;
    if(name === 'remember') {
      return this.props.dispatch(setLoginFormDetails(name, checked));
    }
    this.props.dispatch(setLoginFormDetails(name, value));
  }

  render() {
    return (
      <SignInForm
        onChange={this.handleOnChange}
        {...this.props}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
  };
}

export default withRouter(connect(mapStateToProps)(SignInFormContainer));
