// since the api is going to be asynchronous
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const INITIALIZE_COOKIE = 'INITIALIZE_COOKIE';
export const INITIALIZE_NO_COOKIE = 'INITIALIZE_NO_COOKIE';
export const RESET_AUTH = 'RESET_AUTH';
export const INVALID_TOKEN = 'INVALID_TOKEN';

export const SET_LOGIN_DETAILS = 'SET_LOGIN_DETAILS';

// Actions

// function requestLogin() {
//   return {
//     type: LOGIN_REQUEST,
//     isFetching: true,
//     isAuthenticated: false,
//   };
// }

function onChange(label, value) {
  return {
    type: SET_LOGIN_DETAILS,
    label,
    value,
  };
}

// function receiveLogin(user) {
//   return {
//     type: LOGIN_SUCCESS,
//     isFetching: false,
//     isAuthenticated: true,
//     id_token: user.token,
//     decoded: user.decoded,
//   };
// }

// function loginError(error = 'User ID or password is incorrect') {
//   return {
//     type: LOGIN_FAIL,
//     isFetching: false,
//     isAuthenticated: false,
//     message: error,
//   };
// }

// function receiveLogout() {
//   return {
//     type: LOGOUT_SUCCESS,
//     isFetching: false,
//     isAuthenticated: false,
//   };
// }

// function resetAuth() {
//   return {
//     type: RESET_AUTH,
//     isFetching: false,
//     isAuthenticated: false,
//   };
// }

// function initializeUser(cred) {
//   if (cred.cookie) {
//     return {
//       type: INITIALIZE_COOKIE,
//       isFetching: false,
//       isAuthenticated: true,
//       id_token: cred.cookie,
//       decoded: cred.decoded,
//     };
//   }
//   return {
//     type: INITIALIZE_NO_COOKIE,
//     isFetching: false,
//     isAuthenticated: false,
//     id_token: '',
//     decoded: '',
//   };
// }

// function invalidToken() {
//   return {
//     type: INVALID_TOKEN,
//   };
// }

// +++++++++++++++++++++++

// ++++++++++++++++++++++++++


// ACTION CREATORS

// export function logoutUser() {
//   return (dispatch) => {
//     deleteAllCookies();
//     dispatch(resetUsersState());
//     dispatch(clearAppIdState());
//     dispatch(clearAccountsState());
//     dispatch(clearSignInForm());
//     return dispatch(receiveLogout());
//   };
// }

// export function clearbefore() {
//   return (dispatch) => {
//     dispatch(resetUsersState());
//     dispatch(clearAppIdState());
//     dispatch(clearAccountsState());
//     return dispatch(receiveLogout());
//   };
// }

// export function fetchUserFromToken(cred) {
//   return (dispatch) => {
//     return dispatch(initializeUser(cred));
//   };
// }

// export function loginUser(creds, history) {
//   return (dispatch) => {
//     dispatch(requestLogin());
//         // history.push(redirectUrl);
//         // return dispatch(receiveLogin(decodedToken));
//       });
//     });
//   };
// }

// export function resetSignInForm() {
//   return (dispatch) => {
//     dispatch(resetAuth());
//   };
// }

// export function handleInvalidToken() {
//   return (dispatch) => {
//     dispatch(invalidToken());
//   };
// }

export function setLoginFormDetails(label, value) {
  return (dispatch) => {
    dispatch(onChange(label, value));
  };
}

// +++++++++++++++++++++++
