import { SET_LOGIN_DETAILS } from './SignInFormAction';

const initialState = {
  isFetching: false,
  isFetched: false,
  isAuthenticated: false,
  errorMessage: '',
  id_token: '',
  email: '',
  password: '',
  decoded: { name: '', userId: '', role: { id: '', permissions: {} } },
};

export default function auth(state = initialState, action) {
  switch (action.type) {
    case SET_LOGIN_DETAILS:
      return { ...state, [action.label]: action.value };
    default:
      return state;
  }
}
