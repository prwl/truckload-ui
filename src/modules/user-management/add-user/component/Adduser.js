import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import Card from '@material-ui/core/Card';
import { TextField, ButtonComponent, Dropdown } from '../../../../components';

const styles = {
  card: {
    width: '40%',
    margin: '10px auto',
  },
};

export class AddUser extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;
    return (
      <Card className={classes.card}>
        <CardContent>
          <div align="center">
            <div>
              <h3>
                ADD NEW USER
              </h3>
            </div>
            <div>
              <div>
                <TextField
                  label="First name"
                  placeholder="First name"
                  TextFieldValue=""
                  onChnage={null}
                  isRequired
                />
                <TextField
                  label="Last name"
                  placeholder="Last name"
                  TextFieldValue=""
                  onChnage={null}
                  isRequired
                />
                <TextField
                  label="Email"
                  placeholder="Email"
                  TextFieldValue=""
                  onChnage={null}
                  isRequired
                />
                <Dropdown
                  label="Type"
                />
                <TextField
                  label="Password"
                  placeholder="First name"
                  TextFieldValue=""
                  hint="(6 characters minimum)"
                  onChnage={null}
                  isRequired
                />
                <TextField
                  label="Password confirmation"
                  placeholder="Password confirmation"
                  TextFieldValue=""
                  onChnage={null}
                  isRequired
                />
              </div>
              <div>
                <ButtonComponent
                  value="Add user"
                  color="primary"
                />
                <ButtonComponent
                  value="Clear form"
                  color="secondary"
                />
              </div>
            </div>
          </div>
        </CardContent>
      </Card>
    );
  }
}

export default withStyles(styles)(AddUser);

AddUser.propTypes = {
  classes: PropTypes.object.isRequired,
};
