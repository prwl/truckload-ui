import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Link } from 'react-router-dom';
import { ExitToApp } from '@material-ui/icons/';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  appBar: {
    position: 'relative',
  },
  icon: {
    marginRight: theme.spacing.unit * 2,
  },
  heroUnit: {
    backgroundColor: theme.palette.background.paper,
  },
  heroContent: {
    maxWidth: 600,
    margin: '0 auto',
    padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
  },
  heroButtons: {
    marginTop: theme.spacing.unit * 4,
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
    // height: '100vh',
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 8}px 0`,
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    left: 0,
    bottom: 0,
    width: '100%',
  },
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    minHeight: '75vh',
  },
  container: {
    marginTop: '20px',
    minHeight: '430px',
  },
  footerText: {
    width: '100%',
  },
});

function Navigation(props) {
  const { classes, children } = props;
  return (
    <React.Fragment>
      <CssBaseline />
      <div className={classes.mainDiv}>
        <div>
          <AppBar color="primary" position="static">
            <Toolbar>
              <Typography variant="h4" color="inherit" style={{ fontWeight: 'bold' }}>
                ONCORE
              </Typography>
              <Typography variant="p" color="inherit" style={{ margin: '20px', fontSize: '0.8em', width: '80%' }}>
                Welcome, Narbeh K
              </Typography>
              <Tabs
                value={null}
                onChange={null}
                textColor="inherit"
                style={{ width: '100%' }}
              >
                <Tab label="Home" key={5} value={5} component={Link} to="/login" />
                <Tab label="Reports" key={6} value={6} component={Link} to="/login" />
                <Tab label="Manage Users" key={6} value={6} component={Link} to="/login" />
                <Tab label="Logout" key={6} value={6} component={() => <ExitToApp fontSize="default" style={{ position: 'relative', top: '12px' }} />} to="/login" />
              </Tabs>
            </Toolbar>
          </AppBar>
        </div>
        <div className={classes.container}>
          <Paper className={classes.root} elevation={1}>
            { children }
          </Paper>
        </div>
      </div>
      <div className={classes.footer}>
        <AppBar position="static">
          <Toolbar>
            <Typography color="inherit" align="center" className={classes.footerText}>
                &copy; copyright at TruckBoard 2019
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
    </React.Fragment>
  );
}

Navigation.propTypes = {
  classes: PropTypes.shape().isRequired,
};

export default withStyles(styles)(Navigation);
