import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MUIDataTable from "mui-datatables";
// import ConfirmationDialog from './ConfirmationDialog';
// import FlashMessage from './FlashMessage';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from "@material-ui/icons/Edit";
import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom";
// import {
//     getData,
//     getFilteredData,
//     deleteData,
//     setDeleteData,
//     setDialogClose,
//     handleFlashClosure,
//     getItemDetails,
//     resetState
// } from "../actions";

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    button: {
        margin: theme.spacing.unit,
    },
});

class List extends Component {
    // componentDidMount() {
    //     this.props.resetState();
    //     this.props.getData();
    // }

    // changePage = (page, rowsPerPage, activeColumn = "", searchText = null) => {
    //     const { name = "", sortDirection = "" } = activeColumn;
    //     this.props.getFilteredData({ rowsPerPage, name, page, sortDirection, searchText })
    // };

    // handleDialogClose = () => {
    //     this.props.setDialogClose();
    // };

    // deleteItems = (ids) => {
    //     const { page, rowsPerPage } = this.props;
    //     this.props.deleteData(ids, page, rowsPerPage);
    // }

    // deleteSingleItem = (id) => {
    //     let ids = [id];
    //     this.props.setDeleteData({
    //         dialog: true,
    //         ids,
    //         flashMess: {
    //             type: "info",
    //             enable: false
    //         }
    //     })
    // }

    // handleFlashMessageClosure = () => {
    //     this.props.handleFlashClosure();
    // }

    render() {
        // const { classes, page, pData, count, rowsPerPage, dialog, ids, flashMess } = this.props;
        const columns = ["First Name", "Last Name", "Email", "Account Type",
            {
                name: "Actions",
                options: {
                    filter: false,
                    sort: false,
                    customBodyRender: (value, tableMeta, updateValue) => {
                        return (
                            <Fragment>
                                {/* <IconButton aria-label="Search">
                                    <Link to={`view/${value}`}><SearchIcon color="primary" /></Link>
                                </IconButton> */}
                                <IconButton aria-label="Edit">
                                    <Link to={`edit/${value}`}><EditIcon color="secondary" /></Link>
                                </IconButton>
                                <IconButton aria-label="Delete">
                                    <DeleteIcon onClick={() => this.deleteSingleItem(value)} />
                                </IconButton>
                            </Fragment>
                        );
                    },
                }
            },
        ];

    const options = {
        filter: true,
        filterType: 'dropdown',
        responsive: 'stacked',
        serverSide: true,
        search: true,
        // count,
        // page,
        // rowsPerPage,
        onRowsDelete: (data) => {
            // let ids = data.data.map(item => pData[item.index]._id)
            // this.props.setDeleteData({
            //     dialog: true,
            //     ids,
            //     flashMess: {
            //         type: "info",
            //         enable: false
            //     }
            // })
        },
        onTableChange: (action, tableState) => {
            switch (action) {
                case 'changeRowsPerPage':
                case 'sort':
                case 'search':
                case 'filterChange':
                case 'resetFilters':
                case 'changePage':
                    // this.changePage(tableState.page, tableState.rowsPerPage,
                    //     tableState.columns[tableState.activeColumn], tableState.searchText);
                    break;
                default:
                    break;
            }
        },
        customToolbar:  () => ( 
        <Button variant="contained" color="primary" size="medium    ">
            Add new user
        </Button>
        )
    };
        let pData = [
            {11: 'Frank', 21: 'Jefferson', 31: 'sujie@logistics.com', 41: 'dispatcher'},
            {11: 'Frank', 21: 'Jefferson', 31: 'sujie@logistics.com', 41: 'dispatcher'},
            {11: 'Frank', 21: 'Jefferson', 31: 'sujie@logistics.com', 41: 'dispatcher'},
            {11: 'Frank', 21: 'Jefferson', 31: 'sujie@logistics.com', 41: 'dispatcher'},
        ];
        let a = pData.map(item => {
            return Object.values(item);
        });

        if (a.length > 0) {
            a.map(item => {
                return item.push(item[0]);
            });
        }

        let anchorOrigin = {
            vertical: 'bottom',
            horizontal: 'center',
        }

        return (
            <div className={null}>
                {/* {flashMess.enable && (flashMess.type === "info" ?
                    <FlashMessage variant="success" message="Item deleted successfully" anchor={anchorOrigin} onClose={this.handleFlashMessageClosure} /> :
                    <FlashMessage variant="error" message="Unknown error occurred" anchor={anchorOrigin} onClose={this.handleFlashMessageClosure} />
                )}

                {dialog ? <ConfirmationDialog clickHandler={() => this.deleteItems(ids)}
                    handleClose={this.handleDialogClose} open={dialog}
                    message="Confirm delete operation?" /> : null} */}
                
                <MUIDataTable
                    title={"Manage Users"}
                    data={a}
                    columns={columns}
                    options={options}
                />
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        // page: state.ListReducer.page,
        // pData: state.ListReducer.pData,
        // count: state.ListReducer.count,
        // rowsPerPage: state.ListReducer.rowsPerPage,
        // dialog: state.ListReducer.dialog,
        // ids: state.ListReducer.ids,
        // flashMess: state.ListReducer.flashMess,
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        // getData,
        // getFilteredData,
        // deleteData,
        // setDeleteData,
        // setDialogClose,
        // handleFlashClosure,
        // getItemDetails,
        // resetState
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(List));