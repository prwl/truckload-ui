import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MUIDataTable from "mui-datatables";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Button from '@material-ui/core/Button';
import { MapComponent } from '../../../../components/GoogleMap/GoogleMap';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import FormLabel from '@material-ui/core/FormLabel';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Checkbox from '@material-ui/core/Checkbox';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        width: '50%',
        margin: '10px auto',
      },
    formControl: {
      margin: theme.spacing.unit,
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      width: 200,
    },
    menu: {
    width: 200,
    },
    formControlCheckBox: {
        margin: theme.spacing.unit * 3,
    },
    formControlSelect: {
        margin: theme.spacing.unit,
        minWidth: 150,
        maxWidth: 300,
      },
      chips: {
        display: 'flex',
        flexWrap: 'wrap',
      },
      chip: {
        margin: theme.spacing.unit / 4,
      },
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const names = [
    'Oliver Hansen',
    'Van Henry',
    'April Tucker',
    'Ralph Hubbard',
    'Omar Alexander',
    'Carlos Abbott',
    'Miriam Wagner',
    'Bradley Wilkerson',
    'Virginia Andrews',
    'Kelly Snyder',
  ];
  
  function getStyles(name, that) {
    return {
      fontWeight:
        that.state.name.indexOf(name) === -1
          ? that.props.theme.typography.fontWeightRegular
          : that.props.theme.typography.fontWeightMedium,
    };
  }

class DriverReport extends Component {

    render() {
        const { classes } = this.props;
    //     const columns = [
    //         {
    //             name: 11,
    //             label: 'Last Updated',
    //         }, 
    //         {
    //             name: 21,
    //             label: 'Name',
    //         }, 
    //         {
    //             name: 31,
    //             label: 'Phone',
    //         }, 
    //         {
    //             name: 41,
    //             label: 'Truck Type',
    //         }, 
    //         {
    //             name: 51,
    //             label: 'Date Available',
    //         }, 
    //         {
    //             name: 61,
    //             label: 'Current Location',
    //         },
    //     {
    //         name: 71,
    //         label: 'Desired Location',
    //     },
    //     {
    //         name: 81,
    //         label: 'Active',
    //         options: {
    //             setCellProps: () => {
    //                 return { style: { backgroundColor: 'rgb(152, 224, 188)' }}
    //             }
    //         }
    //     },
    //     {
    //         name: 91,
    //         label: 'Driver Status',
    //     }, 
    //     {
    //         name: 12,
    //         label: 'Company',
    //     }, 
    //     {
    //         name: 22,
    //         label: 'Covered by',
    //     }, 
    //     {
    //         name: 22,
    //         label: 'Preferred Lanes',
    //     },
    //     ];

    // const options = {
    //     filter: true,
    //     filterType: 'dropdown',
    //     serverSide: true,
    //     search: true,
    //     responsive: 'scroll',
    //     fixedHeader: false,
    //     onRowsDelete: (data) => {
    //     },
    //     onTableChange: (action, tableState) => {
    //         switch (action) {
    //             case 'changeRowsPerPage':
    //             case 'sort':
    //             case 'search':
    //             case 'filterChange':
    //             case 'resetFilters':
    //             case 'changePage':
    //                 break;
    //             default:
    //                 break;
    //         }
    //     },
    //     customToolbar:  () => ( 
    //     <Button variant="contained" color="primary" size="medium">
    //         Create New Driver
    //     </Button>
    //     ),
    //   setRowProps: (row) => {
    //     if (row[10] === 'Eric Fastlane') {
    //       return { style: { backgroundColor: 'rgb(219, 107, 107)' }}
    //     }
    //   },

    // };
    // const pData = [
    //   {
    //     11: 'Fri, 02/08/19, 5:10 PM Eric Fastlane',
    //     21: 'CERGIO ARELLANOS(DRIVER)',
    //     31: '(626) 831-3103 - MARIA',
    //     41: '53V',
    //     51: '2019-02-11',
    //     61: 'HOUSTON, Texas',
    //     71: 'CA California',
    //     81: 'Answering',
    //     91: '',
    //     12: 'C&L TRUCKING SERVICE',
    //     22: 'Eric Fastlane',
    //     32: 'California Texas',
    //   },
    //   {
    //     11: 'Fri, 02/08/19, 5:10 PM Eric Fastlane',
    //     21: 'CERGIO ARELLANOS(DRIVER)',
    //     31: '(626) 831-3103 - MARIA',
    //     41: '53V',
    //     51: '2019-02-11',
    //     61: 'HOUSTON, Texas',
    //     71: 'CA California',
    //     81: 'Answering',
    //     91: '',
    //     12: 'C&L TRUCKING SERVICE',
    //     22: 'Eric Fastlane',
    //     32: 'California Texas',
    //   },
    //   {
    //     11: 'Fri, 02/08/19, 5:10 PM Eric Fastlane',
    //     21: 'CERGIO ARELLANOS(DRIVER)',
    //     31: '(626) 831-3103 - MARIA',
    //     41: '53V',
    //     51: '2019-02-11',
    //     61: 'HOUSTON, Texas',
    //     71: 'CA California',
    //     81: 'Answering',
    //     91: '',
    //     12: 'C&L TRUCKING SERVICE',
    //     22: '',
    //     32: 'California Texas',
    //   },
    //   {
    //     11: 'Fri, 02/08/19, 5:10 PM Eric Fastlane',
    //     21: 'CERGIO ARELLANOS(DRIVER)',
    //     31: '(626) 831-3103 - MARIA',
    //     41: '53V',
    //     51: '2019-02-11',
    //     61: 'HOUSTON, Texas',
    //     71: 'CA California',
    //     81: 'Answering',
    //     91: '',
    //     12: 'C&L TRUCKING SERVICE',
    //     22: '',
    //     32: 'California Texas',
    //   },
    //   {
    //     11: 'Fri, 02/08/19, 5:10 PM Eric Fastlane',
    //     21: 'CERGIO ARELLANOS(DRIVER)',
    //     31: '(626) 831-3103 - MARIA',
    //     41: '53V',
    //     51: '2019-02-11',
    //     61: 'HOUSTON, Texas',
    //     71: 'CA California',
    //     81: 'Answering',
    //     91: '',
    //     12: 'C&L TRUCKING SERVICE',
    //     22: '',
    //     32: 'California Texas',
    //   },
    // ];
    const currencies = [
        // {
        //   value: 'USD',
        //   label: '$',
        // },
        // {
        //   value: 'EUR',
        //   label: '€',
        // },
        // {
        //   value: 'BTC',
        //   label: '฿',
        // },
        // {
        //   value: 'JPY',
        //   label: '¥',
        // },
      ];
    return (
      <div className={classes.container}>
          <TextField
          id="driver_name"
          label="Driver Name"
        //   type="search"
          className={classes.textField}
          margin="normal"
        />
          <TextField
          id="driver_phone"
          label="Driver Phone"
        //   type="search"
          className={classes.textField}
          margin="normal"
        />
          <TextField
          id="driver_id"
          label="Driver ID"
        //   type="search"
          className={classes.textField}
          margin="normal"
        />
          <TextField
          id="trailer_type"
          select
          label="Trailer Type"
          className={classes.textField}
          value={null}
          onChange={null}
          SelectProps={{
            native: true,
            MenuProps: {
              className: classes.menu,
            },
          }}
          helperText="Please select your trailer type"
          margin="normal"
        >
          {currencies.map(option => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </TextField>
          <TextField
          id="current_state"
          select
          label="Current State"
          className={classes.textField}
          value={null}
          onChange={null}
          SelectProps={{
            native: true,
            MenuProps: {
              className: classes.menu,
            },
          }}
          helperText="Please select your current state"
          margin="normal"
        >
          {currencies.map(option => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </TextField>
          <TextField
          id="active"
          select
          label="Active"
          className={classes.textField}
          value={null}
          onChange={null}
          SelectProps={{
            native: true,
            MenuProps: {
              className: classes.menu,
            },
          }}
          helperText="Please select your status"
          margin="normal"
        >
          {currencies.map(option => (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          ))}
        </TextField>
        <TextField
          id="current_city"
          label="Current City"
        //   type="search"
          className={classes.textField}
          margin="normal"
        />
        <FormControl component="fieldset" className={classes.formControlCheckBox}>
          <FormGroup>
            <FormControlLabel
              control={
                <Checkbox />
              }
              label="Plate Trailer"
            />
          </FormGroup>
        </FormControl>
        <FormControl component="fieldset" className={classes.formControlCheckBox}>
          <FormGroup>
            <FormControlLabel
              control={
                <Checkbox />
              }
              label="E-Trac"
            />
          </FormGroup>
        </FormControl>
        <FormControl component="fieldset" className={classes.formControlCheckBox}>
          <FormGroup>
            <FormControlLabel
              control={
                <Checkbox />
              }
              label="Blackhaul"
            />
          </FormGroup>
        </FormControl>
        <TextField
          id="radius"
          label="Radius (Mi)"
        //   type="search"
          className={classes.textField}
          margin="normal"
        />
        <TextField
          id="company_name"
          label="Company Name"
        //   type="search"
          className={classes.textField}
          margin="normal"
        />
        <TextField
          id="driver_avail"
          label="Driver Availability"
        //   type="search"
          className={classes.textField}
          margin="normal"
        />
        <FormControl className={classes.formControlSelect}>
          <InputLabel htmlFor="preferred_lanes">Preferred Lanes</InputLabel>
          <Select
            multiple
            value={[]}
            onChange={null}
            input={<Input id="preferred_lanes" />}
            renderValue={selected => (
              <div className={classes.chips}>
                {selected.map(value => (
                  <Chip key={value} label={value} className={classes.chip} />
                ))}
              </div>
            )}
            MenuProps={MenuProps}
          >
            {names.map(name => (
              <MenuItem key={name} value={name} style={null}>
                {name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl className={classes.formControlSelect}>
          <InputLabel htmlFor="dest_zones">Destination Zones</InputLabel>
          <Select
            multiple
            value={[]}
            onChange={null}
            input={<Input id="dest_zones" />}
            renderValue={selected => (
              <div className={classes.chips}>
                {selected.map(value => (
                  <Chip key={value} label={value} className={classes.chip} />
                ))}
              </div>
            )}
            MenuProps={MenuProps}
          >
            {names.map(name => (
              <MenuItem key={name} value={name} style={null}>
                {name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl className={classes.formControlSelect}>
          <InputLabel htmlFor="current_zones">Current Zones</InputLabel>
          <Select
            multiple
            value={[]}
            onChange={null}
            input={<Input id="current_zones" />}
            renderValue={selected => (
              <div className={classes.chips}>
                {selected.map(value => (
                  <Chip key={value} label={value} className={classes.chip} />
                ))}
              </div>
            )}
            MenuProps={MenuProps}
          >
            {names.map(name => (
              <MenuItem key={name} value={name} style={null}>
                {name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        {/* 
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="preferred_lanes">Preferred Lanes</InputLabel>
          <Input id="preferred_lanes" value={null} onChange={this.handleChange} />
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="dest_zones">Destination Zones</InputLabel>
          <Input id="dest_zones" value={null} onChange={this.handleChange} />
        </FormControl>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="current_zones">Current Zones</InputLabel>
          <Input id="current_zones" value={null} onChange={this.handleChange} />
        </FormControl> */}
      </div>
    );
    }
}

const mapStateToProps = state => {
    return {
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(DriverReport));
