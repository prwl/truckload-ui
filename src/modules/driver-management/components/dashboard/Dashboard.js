import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import MUIDataTable from "mui-datatables";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Button from '@material-ui/core/Button';
import { MapComponent } from '../../../../components/GoogleMap/GoogleMap';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    button: {
        margin: theme.spacing.unit,
    },
});

class Dashboard extends Component {

    render() {
        const columns = [
            {
                name: 11,
                label: 'Last Updated',
            }, 
            {
                name: 21,
                label: 'Name',
            }, 
            {
                name: 31,
                label: 'Phone',
            }, 
            {
                name: 41,
                label: 'Truck Type',
            }, 
            {
                name: 51,
                label: 'Date Available',
            }, 
            {
                name: 61,
                label: 'Current Location',
            },
        {
            name: 71,
            label: 'Desired Location',
        },
        {
            name: 81,
            label: 'Active',
            options: {
                setCellProps: () => {
                    return { style: { backgroundColor: 'rgb(152, 224, 188)' }}
                }
            }
        },
        {
            name: 91,
            label: 'Driver Status',
        }, 
        {
            name: 12,
            label: 'Company',
        }, 
        {
            name: 22,
            label: 'Covered by',
        }, 
        {
            name: 22,
            label: 'Preferred Lanes',
        },
        ];

    const options = {
        filter: true,
        filterType: 'dropdown',
        serverSide: true,
        search: true,
        responsive: 'scroll',
        fixedHeader: false,
        onRowsDelete: (data) => {
        },
        onTableChange: (action, tableState) => {
            switch (action) {
                case 'changeRowsPerPage':
                case 'sort':
                case 'search':
                case 'filterChange':
                case 'resetFilters':
                case 'changePage':
                    break;
                default:
                    break;
            }
        },
        customToolbar:  () => ( 
        <Button variant="contained" color="primary" size="medium">
            Create New Driver
        </Button>
        ),
      setRowProps: (row) => {
        if (row[10] === 'Eric Fastlane') {
          return { style: { backgroundColor: 'rgb(219, 107, 107)' }}
        }
      },

    };
    const pData = [
      {
        11: 'Fri, 02/08/19, 5:10 PM Eric Fastlane',
        21: 'CERGIO ARELLANOS(DRIVER)',
        31: '(626) 831-3103 - MARIA',
        41: '53V',
        51: '2019-02-11',
        61: 'HOUSTON, Texas',
        71: 'CA California',
        81: 'Answering',
        91: '',
        12: 'C&L TRUCKING SERVICE',
        22: 'Eric Fastlane',
        32: 'California Texas',
      },
      {
        11: 'Fri, 02/08/19, 5:10 PM Eric Fastlane',
        21: 'CERGIO ARELLANOS(DRIVER)',
        31: '(626) 831-3103 - MARIA',
        41: '53V',
        51: '2019-02-11',
        61: 'HOUSTON, Texas',
        71: 'CA California',
        81: 'Answering',
        91: '',
        12: 'C&L TRUCKING SERVICE',
        22: 'Eric Fastlane',
        32: 'California Texas',
      },
      {
        11: 'Fri, 02/08/19, 5:10 PM Eric Fastlane',
        21: 'CERGIO ARELLANOS(DRIVER)',
        31: '(626) 831-3103 - MARIA',
        41: '53V',
        51: '2019-02-11',
        61: 'HOUSTON, Texas',
        71: 'CA California',
        81: 'Answering',
        91: '',
        12: 'C&L TRUCKING SERVICE',
        22: '',
        32: 'California Texas',
      },
      {
        11: 'Fri, 02/08/19, 5:10 PM Eric Fastlane',
        21: 'CERGIO ARELLANOS(DRIVER)',
        31: '(626) 831-3103 - MARIA',
        41: '53V',
        51: '2019-02-11',
        61: 'HOUSTON, Texas',
        71: 'CA California',
        81: 'Answering',
        91: '',
        12: 'C&L TRUCKING SERVICE',
        22: '',
        32: 'California Texas',
      },
      {
        11: 'Fri, 02/08/19, 5:10 PM Eric Fastlane',
        21: 'CERGIO ARELLANOS(DRIVER)',
        31: '(626) 831-3103 - MARIA',
        41: '53V',
        51: '2019-02-11',
        61: 'HOUSTON, Texas',
        71: 'CA California',
        81: 'Answering',
        91: '',
        12: 'C&L TRUCKING SERVICE',
        22: '',
        32: 'California Texas',
      },
    ];

    return (
        <div>
            <MapComponent isMarkerShown={true} />
            <MUIDataTable
                title={"Driver Feed"}
                data={pData}
                columns={columns}
                options={options}
            />
        </div>
    );
    }
}

const mapStateToProps = state => {
    return {
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Dashboard));
