import React from 'react';

import {
  Switch,
  Route,
  withRouter,
  Redirect,
} from 'react-router-dom';

import SignInFormContainer from '../../public-modules/SignInForm/SignInFormContainer';

const PublicRoute = ({ component: Component, isAuthenticated, ...rest }) => {
  // const redirectUrl = '/login';
  const redirectUrl = '/add-user';
  return (
    <Route
      {...rest}
      render={props => (
        !isAuthenticated
          ? <Component {...props} />
          : <Redirect to={redirectUrl} />
      )}
    />
  );
};

const NewPublicRoute = withRouter(PublicRoute);

const PublicComponent = () => {
  return (
    <div>
      <Switch>
        <NewPublicRoute exact path="/login" component={SignInFormContainer} isAuthenticated={false} />
      </Switch>
    </div>
  );
};

export default withRouter(PublicComponent);
