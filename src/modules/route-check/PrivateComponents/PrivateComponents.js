import React, { PureComponent } from 'react';
import { Switch, withRouter, Route } from 'react-router-dom';
import List from '../../../modules/driver-management/components/list-users/List';
import AddUserContainer from '../../../modules/user-management/add-user/AddUserContainer';
import Dashboard from '../../../modules/driver-management/components/dashboard/Dashboard';
import DriverReport from '../../../modules/driver-management/components/driver-reports/DriverReport';
import HeaderHoc from '../../../components/HOC/HeaderHOC';
import Layout from '../../driver-management/Layout/Layout';

const listComponent = HeaderHoc(Layout, List);
const addUserComponent = HeaderHoc(Layout, AddUserContainer);
const dashboardComponent = HeaderHoc(Layout, Dashboard);
const driverReportComponent = HeaderHoc(Layout, DriverReport);

export class PrivateComponent extends PureComponent {
  componentWillMount() {

  }

  render() {
    return (
      <div>
        <Switch>
          <Route path="/list" component={listComponent} />
          <Route path="/add-user" component={addUserComponent} />
          <Route path="/dashboard" component={dashboardComponent} />
          <Route path="/driver-report" component={driverReportComponent} />
        </Switch>
      </div>
    );
  }
}

export default withRouter(PrivateComponent);
