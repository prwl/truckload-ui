import React, { Component } from 'react';

import { PublicComponent } from './modules/route-check/PublicComponents';
import { PrivateComponent } from './modules/route-check/PrivateComponents';

class App extends Component {
  render() {
    return (
      <div>
        <PublicComponent />
        <PrivateComponent />
      </div>
    );
  }
}

export default App;
