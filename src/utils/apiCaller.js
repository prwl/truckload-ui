import fetch from 'isomorphic-fetch';

export const API_URL = '';

export const singleApiAddress = '';

export default function callApi(endpoint, method = 'get', body) {
  return fetch(`${API_URL}/${endpoint}`, {
    headers: {
      'content-type': 'application/json',
      'cache-control': 'no-cache',
      pragma: 'no-cache',
    },
    method,
    credentials: 'same-origin',
    body: JSON.stringify(body),
  })
    .then((response) => {
      return response;
    })
    .then(response => response.json().then(json => ({ json, response })))
    .then(({ json, response }) => {
      const finalJson = Object.assign({}, json, { status: response.status });
      if (!response.ok) {
        return Promise.reject(finalJson);
      }
      return finalJson;
    })
    .then(json => json, (error) => {
      if (error.message === 'Failed to fetch' || error.message === 'Network request failed' || error.message === 'NetworkError when attempting to fetch resource.' || error.message === 'Unexpected token < in JSON at position 0') {
        return {
          status: 503,
          message: 'Your request cannot be processed at this time. Please try in some time.',
        };
      }
      return error;
    });
}
