import React, { Component } from 'react';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const styles = theme => ({
  formControl: {
    margin: theme.spacing.unit,
    'min-width': 300,
  },
});

class TextField extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      label,
      TextFieldValue,
      classes,
    } = this.props;
    return (
      <div>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="component-simple">
            {label}
          </InputLabel>
          <Input
            id="component-simple"
            value={TextFieldValue}
            onChange={null}
          />
        </FormControl>
      </div>
    );
  }
}

export default withStyles(styles)(TextField);

TextField.defaultProps = {
  label: '',
  TextFieldValue: '',
  isRequired: false,
};

TextField.propTypes = {
  isRequired: PropTypes.bool,
  label: PropTypes.string,
  TextFieldValue: PropTypes.string,
  classes: PropTypes.object.isRequired,
};
