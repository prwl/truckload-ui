import React from 'react';

const HeaderHOC = (Navigation, Component) => {
  // React component returned by HOC ++++++++++++++++++++++++++++
  const WrapperComponent = () => {
    return (
      <Navigation><Component /></Navigation>
    );
  };
  return WrapperComponent;
};

export default HeaderHOC;
