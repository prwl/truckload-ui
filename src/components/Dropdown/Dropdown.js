import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import PropTypes from 'prop-types';

const styles = theme => ({
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 300,
  },
});


export class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;
    const { label, value } = this.props;
    return (
      <div>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="type-native-simple">{label}</InputLabel>
          <Select
            native
            value={value}
            onChange={null}
            inputProps={{
              name: { label },
              id: 'type-native-simple',
            }}
          >
            <option value="" />
            <option value={10}>dispatcher</option>
            <option value={20}>Twenty</option>
            <option value={30}>Thirty</option>
          </Select>
        </FormControl>
      </div>
    );
  }
}
export default withStyles(styles)(Dropdown);

Dropdown.defaultProps = {
  label: '',
  value: '',
  color: '',
};

Dropdown.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  classes: PropTypes.object.isRequired,
};
