import React from 'react';
import { compose, withProps } from 'recompose';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps';

export const MapComponent = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyC_Z15TdQ26UaGsByIdvD2d8PtKQ6nx7HU&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `700px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)((props) =>
  <GoogleMap
    // ref={map => map && map.fitBounds(props.bounds)}
    defaultZoom={4.7}
    defaultCenter={{ lat: 39.110298, lng: -94.581078 }}
  >
    {props.isMarkerShown && <Marker position={{ lat: 39.110298, lng: -94.581078 }} onClick={props.onMarkerClick} />}
  </GoogleMap>
)