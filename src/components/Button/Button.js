import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});

export class ButtonComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;
    const { color, value } = this.props;
    return (
      <Button variant="contained" color={color} className={classes.button}>
        {value}
      </Button>
    );
  }
}
export default withStyles(styles)(ButtonComponent);

ButtonComponent.defaultProps = {
  value: '',
  color: '',
};

ButtonComponent.propTypes = {
  value: PropTypes.string,
  color: PropTypes.string,
  classes: PropTypes.object.isRequired,
};

