import React from 'react';
import { css } from '@emotion/core';
import { BeatLoader } from 'react-spinners';

const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
`;

export default class Loader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  render() {
    const { loading } = this.state;
    return (
      <div className="sweet-loading">
        <BeatLoader
          css={override}
          sizeUnit="px"
          size={15}
          color="#123abc"
          loading={loading}
        />
      </div>
    );
  }
}
